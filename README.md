
# Flo Water Jug Challenge

### Challenge:
Build an application that solves the Water Jug Riddle for dynamic inputs (X, Y, Z). The simulation
should have a UI to display state changes for each state for each jug (Empty, Full or Partially Full).

You have an X-gallon and a Y-gallon jug that you can fill from a lake. (Assume lake has unlimited
amount of water.) By using only an X-gallon and Y-gallon jug (no third jug), measure Z gallons of
water. 

### Goals:
1. Measure Z gallons of water in the most efficient way.
2. Build a UI where a user can enter any input for X, Y, Z and see the solution.
3. If no solution, display “No Solution”.


### Solution:
[https://us-central1-hip-river-247606.cloudfunctions.net/compare?x={{x}}&y={{y}}&z={{z}}](https://us-central1-hip-river-247606.cloudfunctions.net/compare?x={{x}}&y={{y}}&z={{z}})

Query Params:
- x: int (required)
- y: int (required)
- z: int (required)

Returns: Array of arrays of steps displaying the fill/empty/transfer of water between jugs

```
steps: [ [ 4, 0 ], [ 0, 4 ], [ 4, 4 ], [ 0, 8 ], [ 4, 8 ], [ 1, 11 ] ]
```

Refers to:
1. Fill 4 gallon jug
2. Transfer 4 gallons to 11 gallon (4/11)
3. Fill 4 gallon jug
4. Transfer 4 gallons to 11 gallon (8/11)
5. Fill 4 gallon jug
6. Tranfer 3 gallons to 11 gallons (11/11)

`steps.length` will display the minimum number of steps required

### Deployment
GCP cloud function using Serverless CLI tools 

### 