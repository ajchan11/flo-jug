const validate = require('validate.js')

const validationParams = {
  x: {
    presence: {
      allowEmpty: false
    },
    type: "number"
  },
  y: {
    presence: {
      allowEmpty: false
    },
    type: "number"
  },
  z: {
    presence: {
      allowEmpty: false
    },
    type: "number"
  }
}


// use GCD to determine if answer is viable
const greatestCommonDenominator = (a, b) => {
  // if b is 0, a must be the GCD
  if (b === 0) {
    return a
  }
  // recursively get GCD, will eventually return a number
  return greatestCommonDenominator(b, a % b); 
}

const buildSteps = (x, y, z) => {
  // because z % gcd.value === 0 -> there has to be a solution
  // can loop over infinite times until we find solution 
  // setup inital jug using volume x
  let from = x; 
  let to = 0;

  // create the first "step"
  const steps = [[from, to]]

  // forever loop until the target volume is reach
  while (from !== z && to !== z) { 
    // max amount that can be poured 
    const hold = Math.min(from, y - to); 

    // pour "hold" volume in from to target to
    to   += hold; 
    from -= hold; 
    // push the step
    steps.push([from, to])

    // escape the loop if target volume is reached by either
    if (from == z || to == z)  {
      break; 
    }
        

    // if from jug is empty, set to x volume
    if (from == 0) { 
      from = x; 
      steps.push([from, to])
    } 

    // if to jug is empty, set to y volume
    if (to == y) { 
      to = 0;
      steps.push([from, to])
    } 
  } 
  return steps
}


exports.compare = (request, response) => {

  const { query } = request

  // needing to type cast query parameters for validation and comparisson
  const params = {
    x: +query.x,
    y: +query.y,
    z: +query.z
  }

  // validation and type casting 
  const invalid = validate(params, validationParams)

  if (invalid) {
    return response.status(400).send(invalid)
  }

  const { x, y, z } = params 

  return new Promise((resolve, reject) => {
    // if x + y < z, false right away
    if (x + y < z) {
      reject({
        errors: [`No Solution - ${x} + ${y} is less than ${z}`]
      })
    }

    // if just x or y or the sum of x and y is equal to z, correct right away
    if (x === z) {
      resolve({
        steps: [[x, 0]]
      }) 
    }

    if (y === z) {
      resolve({
        steps: [[0, y]]
      })
    }

    if (x + y === z) {
      resolve({
        steps: [[x, y]]
      })
    }

    // if z is divisible by the greatest common denominator wholey w/o a remainder, than its possible
    const gcd = greatestCommonDenominator(x, y)

    if (z % gcd === 0) {
      // run through both scenarios in which x or y is filled first
      const xOption = buildSteps(x, y, z)
      const yOption = buildSteps(y, x, z)

      // option with the shortest number of steps wins
      resolve({
        steps: xOption.length > yOption.length ? yOption : xOption
      })
    } else {
      reject({
        errors: ['No Solution - No common denominator']
      })
    }
  })
  .then((res) => {
    return response.status(200).send(res);
  })
  .catch((err) => response.status(400).send(err))
}

// // test 1
// console.log("TEST 1: true")
// console.log(compareJugs(3,5,4))

// // test 2
// console.log("TEST 2: true")
// console.log(compareJugs(11,4,1))

// // test 3
// console.log("TEST 3: false")
// console.log(compareJugs(4,8,5))